#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
$DIR/cleancmake.sh

export CXX=`which clang++`
export CC=`which clang`

if [ "$CXX" == "" ]; then
    CXX=/usr/local/bin/clang++
fi

if [ "$CC" == "" ]; then
    CC=/usr/local/bin/clang
fi

cmake \
   -DCMAKE_BUILD_TYPE=debug \
   -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
   -DCMAKE_INSTALL_PREFIX=/home/timmy/local \
   $DIR

